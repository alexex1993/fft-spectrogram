#! /usr/bin/gnuplot
set pm3d map
set samples 100000
splot 'text1.txt'
set terminal wxt
set output 'text1.png'
set xlabel 'Time'
set ylabel 'Frequency [THz]'
set cblabel 'Intensity in dB'
set cbrange [-20 to -140]
set xrange [GPVAL_DATA_X_MIN to GPVAL_DATA_X_MAX]
set yrange [0 to 44100]
unset key
replot
unset output
