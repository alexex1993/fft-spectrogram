#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sndfile.h>
#include <math.h>
#include <stdlib.h>
#include "fft.h"
#include "analysis.h"
#include "pipeline.h"
#include "display.h"
#include <utility>
#include <algorithm>
#include <numeric>

using std::cout;
using std::endl;
using std::pair;
using std::vector;
using std::make_pair;
using std::ofstream;
using std::find;
using std::max_element;

char * name;
double FREQ;
unsigned int globalLogN;
ofstream out;
ofstream out_max;
ofstream out_pair;
double gr_freq[] = {100, 350, 1500, 2500, 3300, 4000};


void display() 
{	
	char *infilename ;
	SNDFILE *infile = NULL ;
	FILE *outfile = NULL ;
	SF_INFO sfinfo ;
	//Открываем wav-файл
	infile = sf_open(/*argv [1]*/name, SFM_READ, &sfinfo);
	if(!sf_format_check(&sfinfo))
	{
		cout << "Invalid format of input file!" << endl;
		cout << "Exit." << endl;
		exit(0);
	}	
	
	FREQ = sfinfo.samplerate;
	
	int N = pow(2, globalLogN);
	unsigned int numberOfSamples = N*sfinfo.channels;
	double *samples = new double[numberOfSamples];
	complex *complexData = new complex[numberOfSamples];
	double *window = new double[numberOfSamples];
	
	
	for (int i = 0; i < numberOfSamples; i++) window[i] = 0.5 * (1 - cos( 2 * PI * i / N));

	//Выводим данные о файле
	cout << "Samplerate = " << sfinfo.samplerate << endl;
	cout << "Channels = " << sfinfo.channels << endl;
	cout << "Numbers of a samples in the file " << sfinfo.frames << endl;
	

	vector <double> Hz(numberOfSamples/4);
	vector <double> dB(numberOfSamples/4);
	vector <pair<double, double> > qr_amp(numberOfSamples/4);
	vector <vector <pair <double, double> > > qr_amp_main;
	
	
	out.open("text1.txt");
	out_max.open("text_max.txt");
	out_pair.open("text_pair.txt");
	
	while(sf_seek(infile, 0, SEEK_CUR) <  sfinfo.frames)
	{ 	
		
		
		//dB.clear();
		//Hz.clear();
		qr_amp.clear();
		//Читаем N сэмплов
		sf_readf_double(infile, samples, N);
		
		//Применяем окно
		for (int i = 0; i < numberOfSamples; i++) complexData[i] =  window[i] *  samples[i]; /*complexData[i].m_im*/ 

		CFFT::Forward(complexData, N);

		out << endl;
		//Цикла для задания АЧХ
		for (int i = 0; i < (numberOfSamples/2)/sfinfo.channels ; i++)
		{
			
			unsigned int currsamp = sf_seek(infile, 0, SEEK_CUR);
			double amplitude = complexData[i].amplitude()/N;
			double valueIndB = 20.0f * log10f(amplitude);
			unsigned int rr, gg, bb;
			palette_spectrum((valueIndB + 120)/100, rr, gg, bb);
			glColor3ub(rr, gg, bb);
			
			/*cout << "Hz = " << (i * FREQ / N )/2 << " Sample = (" << 
			currsamp - N << " - " << currsamp << ") Time = (" << (currsamp - N) * 1.0/FREQ << " - " << currsamp * 1.0/FREQ 
			<< "s) dB = " << valueIndB << endl;*/

			//Hz.push_back((double)i * FREQ / N);
			//dB.push_back(valueIndB);
			
			qr_amp.push_back(pair<double, double> ((double)i * FREQ / N, valueIndB));

			//(i * FREQ / N, valueIndB);
			
			out << "Hz = " << i * FREQ / N << " Sample = (" << 
			currsamp - N << " - " << currsamp << ") Time = (" << (currsamp - N)/FREQ << " - " << currsamp * 1.0/FREQ 
			<< "s) dB = " << valueIndB << endl;
			
			//out << (currsamp - N)/FREQ << " " << (i * FREQ / N )*2 << " " << valueIndB << endl;
			glClear(GL_COLOR_BUFFER_BIT);
			glPointSize(SIZE_OF_POINT); 
			glBegin(GL_POINTS); 
		        glVertex2f((double)sf_seek(infile, 0, SEEK_CUR)/sfinfo.frames,(double)i/(N/2*sfinfo.channels));
			
		}
		//Высчитать на каждом пороге математическое ожидание амплитуды.
		//Сравнивать текущее значение с математическим ожидаем данного порога
		//Если значимость больше чем на n% то делам значение максимальным
		
		qr_amp_main.push_back(qr_amp);

		
		/*for(int i = 1, min_hz = gr_freq[i-1], max_hz = gr_freq[i]; i < sizeof(gr_freq)/sizeof(double) ; i++,  min_hz = gr_freq[i-1], max_hz = gr_freq[i]) 
		{
			double loc_max = (find(dB.begin() + min_hz/((double)sfinfo.samplerate/N), dB.begin() + max_hz/((double)sfinfo.samplerate/N), *max_element(dB.begin() + min_hz/((double)sfinfo.samplerate/N), dB.begin() + max_hz/((double)sfinfo.samplerate/N))) - dB.begin())* ((double)sfinfo.samplerate/N);
			double loc_max_amp = *max_element(dB.begin() + min_hz/((double)sfinfo.samplerate/N), dB.begin() + max_hz/((double)sfinfo.samplerate/N));
			out_max << "[" << min_hz << " - " << max_hz << "] Freq = " << loc_max  << " Amp = " << loc_max_amp << endl;
			//cout << "max_hz/(sfinfo.samplerate/N) = " << max_hz/((double)sfinfo.samplerate/N) << endl;
		}
		out_max << endl;*/
		
		
		//double max = (std::find(dB.begin(), dB.end(), *std::max_element(dB.begin(), dB.end())) - dB.begin())* ((double)sfinfo.samplerate/N);
		//cout << "max = " << max << " " << *std::max_element(dB.begin(), dB.end()) << endl;
		//cout << "max = " << *std::max_element(dB.begin(), dB.end());
// 		glColor3ub(1.0, 0.0, 0.0);
// 		glClear(GL_COLOR_BUFFER_BIT);
// 		glPointSize(3);
// 		glBegin(GL_POINTS); 
		
// 		glVertex2d((double)sf_se min_hz/(sfinfo.samplerate/N)ek(infile, 0, SEEK_CUR)/sfinfo.frames, (double)sfinfo.samplerate/max);
		
		//cout << " avg = " << std::accumulate(dB.begin() , dB.end(), 0.0) / dB.size() << endl;
		
		
		
	}
	
	glEnd();
	analysis(qr_amp_main);
	
	delete[] complexData;
	
	glFlush();
}