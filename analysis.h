#ifndef ANALISYS
#define ANALISYS

#include <vector>
#include <utility>

extern char * name;
extern double PROCENT_OF_MAX;
void analysis(std::vector <std::vector <std::pair <double, double> > >);

#endif