#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include <utility>
#include <algorithm>
#include <numeric>
#include <stdexcept> 
#include <GL/glut.h>
#include "display.h"

using std::cout;
using std::cerr;
using std::endl;
using std::pair;
using std::vector;
using std::make_pair;
using std::ofstream;
using std::find;
using std::max_element;
using std::for_each;
using std::string;



double PROCENT_OF_MAX;
const int radius = 10;

struct sum_pairs
{
	template<class P, class Q> // или, если очень хочется, параметризовать класс целиком
	P operator() (P const& p, Q const& q) const { return P(p.first+q.first, p.second+q.second); }
};

double check_dispersion(vector <vector <pair <double, double> > > data, int i){
  
	double variance = 0.0;
	double M = accumulate(data[i].begin(), data[i].end(), make_pair(0,0), sum_pairs()).second/(double)data[i].size();
	for(int n = 0; n < data[i].size(); n++)
	{
		variance += pow((data[i][n].second - M), 2.0);
	}
	variance /= (double)data.size();
	//cout << variance << endl;
	return variance;
}



void draw_Peek_Point(float x, float y){

	glPointSize(5.0); 
	glBegin(GL_POINTS);
	//glClear(GL_COLOR_BUFFER_BIT);
	glColor3ub(255,0.0,0);
	glBegin(GL_POINTS); 
	glVertex2f(x, y);
	glEnd();  
	//cout << FREQ << " " <<  x  << " " << y << endl;
}


void analysis(vector <vector <pair <double, double> > > data){
  
	ofstream clear;
	clear.open("Text/cr.txt");
	clear.close();
	
	ofstream cr;
	
	for(int i = 0; i < data.size(); i++){
		int sum_points = 0;
		if(i - radius > 0)
		sum_points = (radius*2 + 1)*(radius*2 + 1) - 1;
		else sum_points = (radius*2 + 1)*(radius*2 + 1) - 1 + (i - radius)*(radius*2 + 1);
		
		if(i + radius >= data.size()) 
		sum_points = (radius*2 + 1)*(radius*2 + 1) - 1 + (i + radius - data.size())*(radius*2 + 1);
		
 		//cout << "Process " << (double)i/data.size() * 100.0 << "%" << endl;
		cr.open("Text/cr.txt", std::ios_base::app);
		cr << endl;
		double curr_variance = check_dispersion(data, i);
		cr << "-----" << endl;
		cr << curr_variance << " #" << i << endl;
		cr << "-----" << endl;
		cr.close();
		
		//if(curr_variance > 60.0)
		for(int j = 20; j < data[i].size() - 2; j++) //Частота от 20*15.25 до 4000-2*15.25
		{
			double point = data[i][j].second;
			double M = 0.0;   
			
			try{
				for(int k = -radius; k < radius + 1; k++){
					for(int n = -radius; n < radius + 1; n++){
						if(!(i + k < 0 || j + n < 0 || i + k >= data.size() || j + n >= data[i].size()))
						  //M += data[i+k][j+n].second; 
						  M += data.at(i+k).at(j+n).second;
					} 
				}
			   }
			catch(const std::out_of_range& oor){cerr << "LOL" << endl;};
	
			M -= point;
			M /= sum_points;
			//cout << "M = " << M << endl;
	
			if(M/point > (PROCENT_OF_MAX/100.0 + 1.0))
			{ 
				//if(i >= 0 && i < 11) cout << "LOLOL" << endl;
				cr.open("Text/cr.txt", std::ios_base::app);
				draw_Peek_Point((float)i/data.size(), (float)data[i][j].first/(FREQ/2.0)); 
				cr << data[i][j].first << " " << data[i][j].second << " " << (M/point - 1.0)*100.0<< "%" << endl;
			}
			//cr << "LOL";
	
			cr.close();
		}	
	}

}

//ffmpeg -i input.mp3 -ss 00:00:00 -to 00:00:20 -c:a libvorbis -ar 8k -ac 1 output.ogg

