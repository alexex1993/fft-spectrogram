reset
set pm3d map
splot 'text1.txt'
set terminal png crop
set output 'text1.png'
set xlabel 'Pulse generator voltage [V]'
set ylabel 'Frequency [THz]'
set cblabel 'Intensity [a.u.]'
set cbrange [0 to GPVAL_DATA_Z_MAX]
set xrange [GPVAL_DATA_X_MIN to GPVAL_DATA_X_MAX]
set yrange [3.2 to 3.45]
unset key
unset output
reset